package org.readium.sdk.android.launcher.util;

import android.os.Handler;
import android.view.ScaleGestureDetector;

import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class ViewScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
    private int MAX_VIDEO_VIEW_HEIGHT = 1920;
    private int MIN_VIDEO_VIEW_HEIGHT = 300;
    private boolean scaling;
    private float scaleFactor = 1.0f;
    private View view;

    public ViewScaleListener(View view) {
        this.view = view;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        scaling = true;
        return true;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        scaling = true;
        ViewGroup.LayoutParams layoutParams = (ViewGroup.LayoutParams) view.getLayoutParams();
        scaleFactor *= detector.getScaleFactor();
        scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));
        // Don't let the object get too small or too large.
        int prevWidth = layoutParams.width;
        int prevHeight = layoutParams.height;
        int newWidth = Math.min(MAX_VIDEO_VIEW_HEIGHT, (int) (layoutParams.width * scaleFactor));
        int newHeight = Math.min(Math.min(MAX_VIDEO_VIEW_HEIGHT, (int) (layoutParams.height * scaleFactor)),MAX_VIDEO_VIEW_HEIGHT);

        if (newHeight == MAX_VIDEO_VIEW_HEIGHT || newHeight == MIN_VIDEO_VIEW_HEIGHT){
            System.out.println("MAX or MIN");
            return false;
        }
        layoutParams.width = newWidth;
        layoutParams.height = newHeight;

        int diffX = newWidth - prevWidth;
        int diffY = newHeight - prevHeight;
        view.animate()
                .x(view.getX() - diffX / 2)
                .y(view.getY() - diffY / 2)
                .setDuration(0)
                .start();
        scaleFactor = 1.f;
        view.setLayoutParams(layoutParams);
        return true;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {
        super.onScaleEnd(detector);
        System.out.println("scale end");

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                scaling = false;
            }
        }, 200);
    }


    public float getScaleFactor() {
        return scaleFactor;
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }
}

