package org.readium.sdk.android.launcher;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;

import org.readium.sdk.android.launcher.util.ViewScaleListener;

public class ViewMoveAndScaleTouchListener implements View.OnTouchListener, GestureDetector.OnGestureListener,GestureDetector.OnDoubleTapListener {
    private final ScaleGestureDetector scaleDetector;
    private final View view;
    private final ViewScaleListener viewScaleListener;
    private final GestureDetector doubleTapGestureDetector;
    float touchX, touchY;
    int pointerCount = 0;
    long lastGestureTimeStamp = 0;
    private static final int defaultViewWidth = 600;
    private static final int defaultViewHeight = 400;
    private static final int defaultViewPositionX = 50;
    private static final int defaultViewPositionY = 50;


    public ViewMoveAndScaleTouchListener(View view) {
        this.viewScaleListener = new ViewScaleListener(view);
        this.scaleDetector = new ScaleGestureDetector(view.getContext(), viewScaleListener);
        doubleTapGestureDetector = new GestureDetector(view.getContext(), this);
        doubleTapGestureDetector.setOnDoubleTapListener(this);
        this.view = view;
        view.setOnTouchListener(this);
    }

    public float getScaleFactor() {
        return viewScaleListener.getScaleFactor();
    }

    public void setScaleFactor(float scalefactor) {
        viewScaleListener.setScaleFactor(scalefactor);
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        doubleTapGestureDetector.onTouchEvent(event);
        float viewWidth = view.getWidth();
        float viewHeight = view.getHeight();

        int containerWidth = ((View) view.getParent()).getWidth();
        int containerHeight = ((View) view.getParent()).getHeight();

        long millisecondsSinceLastGesture = event.getEventTime() - lastGestureTimeStamp;
        lastGestureTimeStamp = event.getEventTime();
        int newPointerCount = event.getPointerCount();
        scaleDetector.onTouchEvent(event);
        //Log.d("scaling", "" + mScaleDetector.isInProgress());
        if (scaleDetector.isInProgress() || newPointerCount > 1) {
            pointerCount = newPointerCount;
            return false;
        }
        //   Log.d("scaling", "not Scalling !!!!!!!!!!!!!!!!!!!!" + pointerCount );
        if (newPointerCount == 1 && pointerCount > 1 && millisecondsSinceLastGesture < 300) {
            pointerCount = newPointerCount;
            touchX = view.getX() - event.getRawX();
            touchY = view.getY() - event.getRawY();
            //   return false;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchX = view.getX() - event.getRawX();
                touchY = view.getY() - event.getRawY();
                break;

            case MotionEvent.ACTION_MOVE:
                float xTranslate = event.getRawX() + touchX;
                float yTranslate = event.getRawY() + touchY;
                if (xTranslate > -(viewWidth / 2) && xTranslate < containerWidth - viewHeight / 2) {
                    view.animate()
                            .x(xTranslate)
                            .setDuration(0)
                            .start();
                }
                if (yTranslate > -(viewHeight / 2) && yTranslate < containerHeight - viewHeight / 2) {
                    view.animate()
                            .y(yTranslate)
                            .setDuration(0)
                            .start();
                }


                break;
            default:
                return false;
        }
        return true;

    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }





    @Override
    public boolean onDoubleTap(MotionEvent e) {
        return doubleTap(e);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return doubleTap(e);
    }

    public boolean doubleTap(MotionEvent e){
        ViewGroup.LayoutParams layoutParams = (ViewGroup.LayoutParams) view.getLayoutParams();
        layoutParams.width = defaultViewWidth;
        layoutParams.height = defaultViewHeight;
        view.setLayoutParams(layoutParams);
        view.setX(defaultViewPositionX);
        view.setY(defaultViewPositionY);
        viewScaleListener.setScaleFactor(1.0f);
        return true;
    }




    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}
